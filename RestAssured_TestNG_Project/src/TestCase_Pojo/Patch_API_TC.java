package TestCase_Pojo;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Pojo.Pojo_Patch_API;
import Pojo.Pojo_Post_API;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Patch_API_TC extends RequestBody {
	ObjectMapper obj_mapper;
	Pojo_Patch_API pojo_object;
	String requestBody;
	@Test
	
	public void Executor()throws ClassNotFoundException, IOException {
		obj_mapper = new ObjectMapper();
		pojo_object = new Pojo_Patch_API();
		pojo_object.setName("Kunal");
		pojo_object.setJob("Lead");
		requestBody = obj_mapper.writeValueAsString(pojo_object);
		File dir_name = Utility.CreateLogDirectory("All_API_Logs");

		String Endpoint = RequestBody.Hostname() + RequestBody.Res_Patch_Update();
		Response response = API_Trigger.Trigger_Patch(RequestBody.HeaderName(), RequestBody.HeaderValue(),
				requestBody, Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("Patch_test_Case"), dir_name, Endpoint,  requestBody,
				response.getHeader("Date"), response.getBody().asString());

		// Extract the response parameters
		int statuscode = response.statusCode();
		
		ResponseBody res_body = response.getBody();
		System.out.println(res_body.asString());
		String res_name = res_body.jsonPath().getString("name");
		String res_job = res_body.jsonPath().getString("job");
		String res_updatedAt = res_body.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);

		// Set the expected results
		JsonPath jsp_req = new JsonPath( requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Validate the response parameters
		Assert.assertEquals(statuscode, 200);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expecteddate);

	}

}
