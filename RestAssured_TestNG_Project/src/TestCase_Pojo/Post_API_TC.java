
package TestCase_Pojo;

import java.io.File;

import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import Pojo.Pojo_Post_API;

public class Post_API_TC extends RequestBody {
	ObjectMapper obj_mapper;
	Pojo_Post_API pojo_object;
	String requestBody;

	@Test
	public void Executor() throws IOException {
		obj_mapper = new ObjectMapper();
		pojo_object = new Pojo_Post_API();
		pojo_object.setName("Sandip");
		pojo_object.setJob("QA");
		requestBody = obj_mapper.writeValueAsString(pojo_object);

		File dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		String Endpoint = RequestBody.Hostname() + RequestBody.Resource();
		Response response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody,
				Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("Post_test_Case"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"),response.getBody().asString());
//	response.getHeader("Date"),
		// Extract the response parameters
		Pojo_Post_API res_body = response.as(Pojo_Post_API.class);
		
		int statuscode = response.statusCode();
		System.out.println(statuscode);
	
		String res_name = res_body.getName();
		System.out.println(res_name);
		String res_job = res_body.getJob();
		System.out.println(res_job);
		String res_id = res_body.getId();
		System.out.println(res_id);
		String res_createdAt = res_body.getCreatedAt();
		res_createdAt = res_createdAt.substring(0, 11);
		System.out.println(res_createdAt);

		// Set the expected results
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Validate the response parameters
		Assert.assertEquals(statuscode, 201);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);

	}

}
