package TestCase_Pojo;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Pojo.Pojo_Post_API;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class TestNG_Annotation_Post_API {
	File dir_name;
	String requestBody;
	String Endpoint;
	Response response;
	ObjectMapper obj_mapper;
	Pojo_Post_API pojo_object;
	
	@BeforeTest
	public void testSetUp() throws IOException {
		System.out.println("BeforeTest Method Called");
		obj_mapper = new ObjectMapper();
		pojo_object = new Pojo_Post_API();
		pojo_object.setName("Sandip");
		pojo_object.setJob("QA");
		requestBody = obj_mapper.writeValueAsString(pojo_object);
		dir_name = Utility.CreateLogDirectory("All_API_Logs");
		Endpoint = RequestBody.Hostname() + RequestBody.Resource();
	}

	@Test(description = "Validate the response body parameters of Post API TestCase1")
	public void validator() {
		System.out.println("Test Method Called");
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody,
				Endpoint);

		int statuscode = response.statusCode();

		ResponseBody res_body = response.getBody();
		System.out.println(res_body.asString());
		String res_name = res_body.jsonPath().getString("name");
		String res_job = res_body.jsonPath().getString("job");
		String res_id = res_body.jsonPath().getString("id");
		String res_createdAt = res_body.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 11);

		// Set the expected results
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Validate the response parameters
		Assert.assertEquals(statuscode, 201);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);

	}
	
	@AfterTest
	
	public  void evidenceCreator() throws IOException {
		System.out.println("AfterTest Method Called");
		Utility.evidenceFileCreator(Utility.testLogName("Post_test_Case"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
	}

}
