
package TestPackage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_Create extends RequestBody {
@Test
	public static void Executor() throws IOException {
		File dir_name = Utility.CreateLogDirectory("All_API_Logs");
		
		String requestBody = RequestBody.req_tc1();  // saving requestbody in variable because it is printing three times and using vriable name 

		String Endpoint = RequestBody.Hostname() + RequestBody.Resource();
		Response response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(),
				requestBody, Endpoint);
		
		
		Utility.evidenceFileCreator(Utility.testLogName("Test_Case_1"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
//	response.getHeader("Date"),
		//Extract the response parameters
		int statuscode = response.statusCode();
	System.out.println(statuscode);
		ResponseBody res_body = response.getBody();
		System.out.println(res_body.asString());
		String res_name = res_body.jsonPath().getString("name");
		String res_job = res_body.jsonPath().getString("job");
		String res_id = res_body.jsonPath().getString("id");
		String res_createdAt = res_body.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 11);

		//Set the expected results
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Validate the response parameters
		Assert.assertEquals(statuscode, 201);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);

	}

}


