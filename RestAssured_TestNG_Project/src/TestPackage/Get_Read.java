package TestPackage;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Get_Read extends RequestBody {
	@Test
	public static void Executor()throws ClassNotFoundException, IOException {

		String Endpoint = RequestBody.Hostname() + RequestBody.Res_Get_ListUser();
		Response response = API_Trigger.Trigger_Get(RequestBody.HeaderName(), RequestBody.HeaderValue(), Endpoint);

		RequestSpecification reqSpec = RestAssured.given();

		 System.out.println(response.getBody().asPrettyString());

		String Res_Body = response.getBody().asString();

		int statusCode = response.statusCode();

		JsonPath res_json = new JsonPath(Res_Body);
		int count = res_json.getInt("data.size()");
		 System.out.println(count);

		int exp_id[] = { 7, 8, 9, 10, 11, 12 };
		String Exp_Email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in", };
		String Exp_FirstName[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String Exp_LastName[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };

		// Fetch data from created array

		int id_Array[] = new int[count];
		String Email_Array[] = new String[count];
		String Fname_Array[] = new String[count];
		String Lname_Array[] = new String[count];

		for (int i = 0; i < count; i++) {

			int res_id = res_json.getInt("data[" + i + "].id");
			id_Array[i] = res_id;

			String res_Email = res_json.getString("data[" + i + "].email");
			Email_Array[i] = res_Email;

			String res_Fname = res_json.getString("data[" + i + "].first_name");
			Fname_Array[i] = res_Fname;

			String res_Lname = res_json.getString("data[" + i + "].last_name");
			Lname_Array[i] = res_Lname;

			Assert.assertEquals(statusCode, 200);
			Assert.assertEquals(exp_id[i], id_Array[i]);
			Assert.assertEquals(Exp_Email[i], Email_Array[i]);
			Assert.assertEquals(Exp_FirstName[i], Fname_Array[i]);
			Assert.assertEquals(Exp_LastName[i], Lname_Array[i]);

		}

	}

}