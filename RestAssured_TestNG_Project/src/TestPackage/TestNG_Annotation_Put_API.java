package TestPackage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class TestNG_Annotation_Put_API {
	File dir_name;
	String requestBody;
	String Endpoint;
	Response response;

	@BeforeTest
	public void testSetUp() throws IOException {
		System.out.println("BeforeTest Method Called");
		dir_name = Utility.CreateLogDirectory("All_API_Logs");
		requestBody = RequestBody.Body_Put_Update();
		Endpoint = RequestBody.Hostname() + RequestBody.Res_Put_Update();
	}

	@Test(description = "Validate the response body parameters of Put API Test Case")
	public void validator() {
		System.out.println("Test Method Called");
		response = API_Trigger.Trigger_Put(RequestBody.HeaderName(), RequestBody.HeaderValue(),
				requestBody, Endpoint);

		int statuscode = response.statusCode();

		ResponseBody res_body = response.getBody();
		System.out.println(res_body.asString());
		String res_name = res_body.jsonPath().getString("name");
		String res_job = res_body.jsonPath().getString("job");
		String res_updatedAt = res_body.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);

		// Set the expected results
		JsonPath jsp_req = new JsonPath( requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Validate the response parameters
		Assert.assertEquals(statuscode, 200);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expecteddate);
	}
	
	@AfterTest
	
	public  void evidenceCreator() throws IOException {
		System.out.println("AfterTest Method Called");
		Utility.evidenceFileCreator(Utility.testLogName("Put_test_Case"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
	}

}
