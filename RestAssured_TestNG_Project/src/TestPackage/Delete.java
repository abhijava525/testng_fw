package TestPackage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Delete extends RequestBody {
	@Test

	public static void Executor()throws ClassNotFoundException, IOException {

		File dir_name = Utility.CreateLogDirectory("All_API_Logs");

		String Endpoint = RequestBody.Hostname() + RequestBody.Source_Delete();
		Response response = API_Trigger.Trigger_Delete(RequestBody.HeaderName(), RequestBody.HeaderValue(),
				 Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("Delete_test_Case_3"), dir_name, Endpoint, RequestBody.Body_Put_Update(),
				response.getHeader("Date"), response.getBody().asString());

		// Extract the response parameters
		int statuscode = response.statusCode();
		System.out.println(statuscode);

		// Validate the response parameters
		Assert.assertEquals(statuscode, 204);
	

	}

}