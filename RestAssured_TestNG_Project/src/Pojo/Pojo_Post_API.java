package Pojo;

public class Pojo_Post_API {

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getJob() {
		return Job;
	}

	public void setJob(String job) {
		Job = job;
	}

	public String getId() {
		return id;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	private String Name;
	private String Job;
	private String id;
	private String createdAt;
}
