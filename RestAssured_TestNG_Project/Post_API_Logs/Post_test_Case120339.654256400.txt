Endpoint is :
https://reqres.in/api/users

Request body is :
{"id":null,"createdAt":null,"name":"Sandip","job":"QA"}

Response body is : 
{"id":"854","createdAt":"2024-03-22T06:33:39.443Z","name":"Sandip","job":"QA"}